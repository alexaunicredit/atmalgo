import pandas as pd
import numpy as np
import os
import sys
import re
import copy
import pickle


folder=r"Journale"

Journale=os.listdir(folder)

Journale=[x for x in Journale if ".txt" in x]

dic={}
for i,Journal in enumerate(Journale):
    
    with open(folder +"\\" + Journal, "r") as f:
        text=f.read()
        
    a=text.split("H001_")    
    del a[0]
    for Automat_text in a:
        Automat=Automat_text[:8]
        if Automat not in dic.keys():
            dic[Automat]={}
            

            
        if True:
            
            date= Automat_text[11:19]

            
            Events=Automat_text.split("----------------------------------")
        
            #if date not in dic[Automat].keys():
            dic[Automat][date]={}
        
            dic[Automat][date]["Anzahl_Events"]=len(Events)
            dic[Automat][date]["Anzahl_Geldbehebung"]=sum([1 for x in Events if "AUSWAHL: AUSZAHLUNG" in x])
            
    print(Journal)

    
    #if i%10==0 and i > 30:
    #    break
    
    #print(i)
    
   
df=pd.DataFrame.from_dict(dic).stack()
df=df.apply(pd.Series)
#df.to_csv("./data/journale.csv")

df.to_pickle("Journale.pkl")
df.reset_index(inplace=True)  
df.columns=["Date","id","Anzahl_Events","Anzahl_Geldbehebung"]
df["Date"]=pd.to_datetime(df['Date'])


